package ua.uraty.logging;

import java.util.Map;
import java.util.WeakHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Dmitry Barannik
 * @since 09.02.18
 */
public interface Logging {

	// INFO
	default void info(String msg, Object... args) {
		StaticLoggersCache.get(this).info(msg, args);
	}
	default void info(String msg) {
		StaticLoggersCache.get(this).info(msg);
	}

	// WARN
	default void warn(String msg, Object... args) {
		StaticLoggersCache.get(this).warn(msg, args);
	}
	default void warn(String msg) {
		StaticLoggersCache.get(this).warn(msg);
	}

	// ERROR
	default void error(String msg, Throwable e) {
		StaticLoggersCache.get(this).error(msg, e);
	}
	default void error(String msg, Object... args) {
		StaticLoggersCache.get(this).error(msg, args);
	}
	default void error(String msg) {
		StaticLoggersCache.get(this).error(msg);
	}

	// DEBUG
	default void debug(String msg, Object... args) {
		StaticLoggersCache.get(this).debug(msg, args);
	}
	default void debug(String msg) {
		StaticLoggersCache.get(this).debug(msg);
	}
}

class StaticLoggersCache {
	private static Map<Class, Logger> loggers = new WeakHashMap<>();
	static Logger get(Object obj) {
		return loggers.computeIfAbsent(obj.getClass(), LoggerFactory::getLogger);
	}

	private StaticLoggersCache() { }
}
