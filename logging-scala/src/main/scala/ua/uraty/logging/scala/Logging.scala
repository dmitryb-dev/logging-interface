package ua.uraty.logging.scala

import org.slf4j.LoggerFactory

/**
  * @author Dmitry Barannik
  * @since 09.02.18
  */
trait Logging {
  private[this] val logger = LoggerFactory.getLogger(this.getClass)

  // Info
  def info(msg: String, args: Any*) = logger.info(msg, args)

  def info(msg: String) = logger.info(msg)

  // Warn
  def warn(msg: String, args: Any*) = logger.warn(msg, args)

  def warn(msg: String) = logger.warn(msg)

  // Error
  def error(msg: String, e: Throwable) = logger.error(msg, e)

  def error(msg: String, args: Any*) = logger.error(msg, args)

  def error(msg: String) = logger.error(msg)

  // Debug
  def debug(msg: String, args: Any*) = logger.debug(msg, args)

  def debug(msg: String) = logger.debug(msg)
}