package ua.uraty.logging.scala

/**
  * @author Dmitry Barannik
  * @since 12.02.18
  */
object LoggingTest extends App with Logging {
  info("Info")
  warn("Warn")
  debug("Debug")
  error("Error")
}
